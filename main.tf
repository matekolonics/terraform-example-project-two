terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "eu-central-1"
}

resource "aws_vpc" "example_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_security_group" "example_sg" {
  vpc_id = aws_vpc.example_vpc.id
}

resource "aws_security_group_rule" "example_sg_rule" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.example_sg.id
  to_port           = 0
  type              = "ingress"
  ipv6_cidr_blocks = [ "::/0" ]
  cidr_blocks = [ "0.0.0.0/0" ]
}

resource "aws_subnet" "example_subnet_1" {
  vpc_id = aws_vpc.example_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-central-1a"
}

resource "aws_subnet" "example_subnet_2" {
  vpc_id = aws_vpc.example_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-central-1b"
}

resource "aws_internet_gateway" "example_igw" {
  vpc_id = aws_vpc.example_vpc.id
}

resource "aws_route_table" "example_rt" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example_igw.id
  }
}

resource "aws_launch_template" "example_launch_template" {
  name_prefix = "example-launch-template-"
  image_id = "ami-0ce5fed0588cd9afe"
  instance_type = "t4g.small"

  vpc_security_group_ids = [aws_security_group.example_sg.id]
}

resource "aws_autoscaling_group" "example_asg" {
  name_prefix = "example-asg-"
  launch_template {
    id = aws_launch_template.example_launch_template.id
    version = "$Latest"
  }
  max_size = 2
  min_size = 1
  desired_capacity = 1
  health_check_grace_period = 300
  health_check_type = "EC2"
  vpc_zone_identifier = [aws_subnet.example_subnet_1.id, aws_subnet.example_subnet_2.id]
}

resource "aws_lb" "example_alb" {
  name = "example-alb"
  internal = false
  load_balancer_type = "application"
  security_groups = [aws_security_group.example_sg.id]
  subnets = [aws_subnet.example_subnet_1.id, aws_subnet.example_subnet_2.id]
}

resource "aws_alb_target_group" "example_target_group" {
  name = "example-tg"
  port = 80
  protocol = "HTTP"
  vpc_id = aws_vpc.example_vpc.id

  health_check {
    path = "/health"
    protocol = "HTTP"
  }
}

resource "aws_alb_listener" "example_listener" {
  load_balancer_arn = aws_lb.example_alb.arn
  port = 80
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_alb_target_group.example_target_group.arn
  }
}

resource "aws_autoscaling_attachment" "example_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.example_asg.id
  lb_target_group_arn = aws_alb_target_group.example_target_group.arn
}

resource "aws_db_subnet_group" "example_rds_subnet_group" {
  name = "example_rds_subnet_group"
  subnet_ids = [aws_subnet.example_subnet_1.id, aws_subnet.example_subnet_2.id]
}

resource "aws_db_instance" "example_rds" {
  instance_class = "db.t4g.micro"
  allocated_storage = "20"
  engine = "postgres"
  db_name = "example"
  username = "root"
  password = "Test1234"
  db_subnet_group_name = aws_db_subnet_group.example_rds_subnet_group.name
  skip_final_snapshot = true
}

resource "aws_s3_bucket" "example_bucket" {
  bucket = "example-terraform-bucket"
}